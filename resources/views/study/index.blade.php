
@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Vista de estudios
</h1>
<h3>
    <?php $study = Session::get('lastStudy');?>
    @if ( Session::has('lastStudy'))
    Ultimo estudio visitado: {{ $study->code }} - {{ $study->name }}
    @endif
</h3>
<p>
<a href="/studies/list">Lista de visitados</a>
   
</p>
@can ('create', \App\Study::class)
<a href="/studies/create">
Alta de estudio
</a>
@else
<!-- No puedes dar de alta :-( -->
@endcan
<table class="table">   

    <tr>
        <th>Id</th>
        <th>Código</th>
        <th>Abreviatura</th>
        <th>Nombre</th>
        <th>Nivel</th>
        <th></th>
    </tr>


@foreach ($studies as $study)
    <tr>
        <td>{{ $study->id }}</td>
        <td>{{ $study->code }}</td>
        <td>{{ $study->abreviation }}</td>
        <td>{{ $study->name }}</td>
        <td>{{ $study->level->name }}</td>
        <td>
        <a href="/studies/{{ $study->id }}">Ver</a>
        <a href="/studies/{{ $study->id }}/edit">Actualizar</a>
        <a href="/studies/{{ $study->id }}/remember">Recordar</a>

        <form action="/studies/{{ $study->id }}" method="post">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        <input type="submit" value="borrar">
        </form>


        </td>

    </tr>
@endforeach
</table>
{{ $studies->links() }}
</div>
@endsection
