@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Lista de estudios visitados:
</h1>
    <?php
        $study = Session::get('lastStudy');
        $studies = Session::get('studies');
    ?>

    @if ( Session::has('lastStudy'))
    Ultimo estudio visitado: 
    {{ $study->code }} - {{ $study->name }}
    @endif


    <h3>Lista de todos</h3>
        <ul>            
        @foreach ($studies as $study)
            <li> {{ $study->name }} </li>
        @endforeach
        </ul>

    <h4><a href="/studies/clearlist">Borrar Lista</a></h4>
</div>
@endsection
