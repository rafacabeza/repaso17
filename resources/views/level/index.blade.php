
@extends('layouts.app')

@section('content')
<div class="container">


<h1>
    Vista de levels
</h1>

<a href="/levels/create">
Alta de estudio
</a>

<table class="table">   

    <tr>
        <th>Id</th>
        <th>Código</th>
        <th>Nombre</th>
        <th></th>
    </tr>


@foreach ($levels as $level)
    <tr>
        <td>{{ $level->id }}</td>
        <td>{{ $level->code }}</td>
        <td>{{ $level->name }}</td>
        <td>
        <a href="/levels/{{ $level->id }}">Ver</a>
        <a href="/levels/{{ $level->id }}/edit">Actualizar</a>

        <form action="/levels/{{ $level->id }}" method="post">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
        <input type="submit" value="borrar">
        </form>


        </td>

    </tr>
@endforeach
</table>
{{ $levels->links() }}
</div>
@endsection
