<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('guest');

Route::get('/parainvitados', function () {
    return "Este contenido es sólo para invitados";
})->middleware('guest');

Route::get('/parausuarios', function () {
    return "Este contenido es sólo para usuarios";
})->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/studies/{id}/modules', 'StudyController@modules');
Route::get('/studies/{id}/remember', 'StudyController@remember');
Route::get('/studies/list', 'StudyController@list');
Route::get('/studies/clearlist', 'StudyController@clearList');
Route::resource('/studies', 'StudyController');
Route::resource('/levels', 'LevelController');

/* Una ruta resource crea una ruta por cada método de
un controlador de tipo resource:
método index:  GET 'studies'
método show:   GET 'studies/{id}'
método create: GET 'studies/create'
método store:  POST 'studies'
*/
