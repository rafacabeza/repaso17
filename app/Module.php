<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = ['code', 'name'];

    public function studies()
    {
        return $this->belongsToMany('App\Study')->withPivot('course');
    }
}
