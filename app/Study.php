<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    protected $fillable = ['code', 'name', 'shortName', 'abreviation', 'level_id'];

    public function level()
    {
        return $this->belongsTo('App\Level');
    }

    public function modules()
    {
        return $this->belongsToMany('App\Module')->withPivot('course');
    }
}
