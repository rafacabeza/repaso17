<?php

namespace App\Policies;

use App\User;
use App\Study;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function view(User $user, Study $study)
    {
        //
    }

    /**
     * Determine whether the user can create studies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->name == 'Juan';
    }

    /**
     * Determine whether the user can update the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function update(User $user, Study $study)
    {
        //
    }

    /**
     * Determine whether the user can delete the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function delete(User $user, Study $study)
    {
        // return $study->id == $user->id;
        return  $user->name == 'Pepe' ||  $user->name == 'Juan';
    }

    public function darbaja(User $user)
    {
        # code...
    }
}
