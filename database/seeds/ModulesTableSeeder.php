<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'code' => '0483',
            'name' => 'Sistemas Informáticos',
            ]);
        DB::table('modules')->insert([
            'code' => '0484',
            'name' => 'Bases de datos',
            ]);
        DB::table('modules')->insert([
            'code' => '0485',
            'name' => 'Programación',
            ]);
        DB::table('modules')->insert([
            'code' => '0486',
            'name' => 'Entornos de Desarrollo',
            ]);
        DB::table('modules')->insert([
            'code' => '0373',
            'name' => 'Lenguajes de Marcas',
            ]);
        DB::table('modules')->insert([
            'code' => 'A471',
            'name' => 'Lengua Extranjera Inglés',
            ]);
    }
}
