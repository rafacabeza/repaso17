<?php

use Illuminate\Database\Seeder;

class StudiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('studies')->insert([
            'id' => 1,
            'code' => 'ESO',
            'name' => 'Educación Secundaria Obligatoria',
            'shortName' => 'Ed. Secundaria',
            'abreviation' => 'ESO',
            'level_id' => 1,
            'created_at' => \Carbon\Carbon::now()
            ]);
        DB::table('studies')->insert([
            'id' => 2,
            'code' => 'BHCS',
            'name' => 'Bachillerato de Humanidades y Ciencias Sociales',
            'shortName' => 'Bachillerato HCS',
            'abreviation' => 'BHCS',
            'level_id' => 2,
            'created_at' => \Carbon\Carbon::now()
            ]);
        DB::table('studies')->insert([
            'id' => 3,
            'code' => 'BCT',
            'name' => 'Bachillerato de Ciencias y Tecnología',
            'shortName' => 'Bachillerato BCT',
            'abreviation' => 'BCT',
            'level_id' => 2,
            'created_at' => \Carbon\Carbon::now()
            ]);
        DB::table('studies')->insert([
            'id' => 4,
            'code' => 'FPB108',
            'name' => 'Formación Profesional Básica de Peluquería y Estética',
            'shortName' => 'FPB de Peluquería y Estética',
            'abreviation' => 'FPB108',
            'level_id' => 3,
            'created_at' => \Carbon\Carbon::now()
            ]);
        DB::table('studies')->insert([
            'id' => 5,
            'code' => 'IMP202',
            'name' => 'Grado Medio de Estética Y Belleza',
            'shortName' => 'GM de Estética',
            'abreviation' => 'GMEB',
            'level_id' => 4,
            'created_at' => \Carbon\Carbon::now()
            ]);
        DB::table('studies')->insert([
            'id' => 6,
            'code' => 'IMP203',
            'name' => 'Grado Medio de Peluquería y Cosmética Capilar',
            'shortName' => 'GM de Peluquería',
            'abreviation' => 'GMPL',
            'level_id' => 4,
            'created_at' => \Carbon\Carbon::now()
            ]);
        DB::table('studies')->insert([
            'id' => 7,
            'code' => 'IMP301',
            'name' => 'Grado Superior de Asesoría de Imagen Personal y Corporativa',
            'shortName' => 'GS de Asesoría de Imagen',
            'abreviation' => 'GSAI',
            'level_id' => 5,
            'created_at' => \Carbon\Carbon::now()
            ]);
        DB::table('studies')->insert([
            'id' => 8,
            'code' => 'IMP302',
            'name' => 'Grado Superior de Estética',
            'shortName' => 'GS de Estética',
            'abreviation' => 'GSES',
            'level_id' => 5,
            'created_at' => \Carbon\Carbon::now()
            ]);
        DB::table('studies')->insert([
            'id' => 9,
            'code' => 'IMP303',
            'name' => 'Grado Superior de Estilismo y Dirección de Peluquería',
            'shortName' => 'GS de Peluquería',
            'abreviation' => 'GSPL',
            'level_id' => 5,
            'created_at' => \Carbon\Carbon::now()
            ]);

        DB::table('studies')->insert([
            'id' => 10,
            'code' => 'IFC201',
            'name' => 'Grado Medio de Sistemas Microinformáticos y Redes',
            'shortName' => 'GM de Informática',
            'abreviation' => 'SMR',
            'level_id' => 4,
            'created_at' => \Carbon\Carbon::now()
            ]);
        DB::table('studies')->insert([
            'id' => 11,
            'code' => 'IFC302',
            'name' => 'Grado Superior de Desarrollo de Aplicaciones Multiplataforma',
            'shortName' => 'GS de D.A. Multiplataforma',
            'abreviation' => 'DAM',
            'level_id' => 5,
            'created_at' => \Carbon\Carbon::now()
            ]);
        DB::table('studies')->insert([
            'id' => 12,
            'code' => 'IFC303',
            'name' => 'Grado Superior de Desarrollo de Aplicaciones Web',
            'shortName' => 'GS de D.A. Web',
            'abreviation' => 'DAW',
            'level_id' => 5,
            'created_at' => \Carbon\Carbon::now()
            ]);

        DB::table('studies')->insert([
            'id' => 13,
            'code' => 'ADG201',
            'name' => 'Grado Medio de Gestión Administrativa',
            'shortName' => 'GM de Administrativo',
            'abreviation' => 'GMGA',
            'level_id' => 4,
            'created_at' => \Carbon\Carbon::now()
            ]);
        DB::table('studies')->insert([
            'id' => 14,
            'code' => 'ADG301',
            'name' => 'Grado Superior de Administración y Finanzas',
            'shortName' => 'GS de Administración y Finanzas',
            'abreviation' => 'GSAF',
            'level_id' => 5,
            'created_at' => \Carbon\Carbon::now()
            ]);
    }
}
