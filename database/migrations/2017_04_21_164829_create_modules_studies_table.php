<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesStudiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_study', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('module_id')->unsigned();
            $table->integer('study_id')->unsigned();
            $table->tinyInteger('course')->unsigned();
            $table->timestamps();
            //foreign keys
            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('study_id')->references('id')->on('studies')->cascade('delete');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_study');
    }
}
